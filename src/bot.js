const mineflayer = require("mineflayer");
const navigate = require("mineflayer-navigate")(mineflayer);

const config = require("../config");
const { host, port, prefix, separator, name, version } = config;

const commands = {};

function command(name, listener, description, aliases) {
    if (commands[name])
        throw new Error(
            "A command with the name " +
                name +
                " already exists! (" +
                JSON.stringify(commands[name]) +
                ")"
        );
    commands[name] = { listener, description, aliases: [] };
    if (aliases)
        aliases.forEach(newAlias => {
            alias(newAlias, name);
        });
}

function alias(alias, name) {
    commands[alias] = {
        aliasOf: name
    };
    commands[name].aliases.push(alias);
}

function executeCommand(user, name, args, res) {
    var command = commands[name];
    if (!command) return res("No command like that");
    if (command.aliasOf) command = commands[command.aliasOf];

    if (command)
        if (command.listener instanceof Function)
            command.listener(user, args, res);
}

function messageReceived(user, msg, plainRes, isConsole) {
    if (!isConsole) if (user == bot.username || user == "you") return;
    if (msg.startsWith(prefix)) {
        const noPrefix = msg.substring(prefix.length);
        const split = noPrefix.split(separator);
        const command = split[0];
        const args = noPrefix
            .substring(command.length + separator.length)
            .split(separator);

        function res(text) {
            if (!text) return;
            var str = "";
            if (text instanceof Object) str = JSON.stringify(text);
            else str = text.toString();

            plainRes(str);
        }

        try {
            executeCommand(user, command, args, res);
        } catch (err) {
            res("Something went wrong..");
            console.log(err);
        }
    } else return;
}

const bot = mineflayer.createBot({
    host,
    port,
    username: name,
    version
});

navigate(bot);

bot.on("chat", (user, msg) => messageReceived(user, msg, bot.chat));
bot.on("whisper", (user, msg) =>
    messageReceived(user, msg, resMsg => bot.whisper(user, resMsg))
);

bot.on("login", () => {
    console.log("Logged in");
});

bot.on("kicked", reason => {
    console.log("Kicked for: " + reason);
});

bot.on("message", msg => console.log(msg.toString()));

const readline = require("readline");
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

consoleChat();

function consoleChat() {
    rl.question("", chat => {
        if (chat.startsWith(prefix))
            messageReceived(name, chat, console.log, true);
        else bot.chat(chat);
        consoleChat();
    });
}

bot.navigate.on("cannotFind", closestPath => {
    bot.navigate.walk(closestPath);
});

bot.navigate.on("interrupted", () => {});

command(
    "help",
    (user, args, res) => {
        var help = "";
        Object.entries(commands).forEach(c => {
            if (c[1].aliasOf) return;
            const description = c[1].description || "No description given";

            help += prefix;
            help += c[0];
            help += ": ";
            help += description;
            if (c[1].aliases) {
                help += " [";
                help += c[1].aliases.join(", ");
                help += "]";
            }
            help += "\n";
        });

        res(help);
    },
    "This help message"
);

module.exports = {
    bot,
    command,
    alias,
    commands,
    config,
    ...bot
};
