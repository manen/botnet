const bot = require("./bot");
const vec3 = require("vec3");
const wiki = require("wikijs").default;
const ping = require("minecraft-server-util");

bot.command(
    "ping",
    (user, args, res) => {
        res(user + ", pong! ");
    },
    "Returns a pong message"
);

bot.command(
    "say",
    (user, args, res) => {
        res(args.join(" "));
    },
    "Forces the bot to say something",
    ["sudo"]
);

bot.command(
    "comehere",
    (user, args, res) => {
        res("Started coming.");
        bot.navigate.to(bot.players[args[0] || user].entity.position);
    },
    "Orders the bot to come to the player",
    ["come", "here"]
);

bot.command(
    "goto",
    (user, args) => {
        bot.navigate.to(vec3(args[0], args[1], args[2]));
    },
    "Orders the bot to go to a specific point",
    ["go", "gotoxyz"]
);

bot.command(
    "test",
    (user, args, res) => {
        res({
            user,
            args
        });
    },
    "Test things"
);

var following = {};
bot.command(
    "follow",
    (user, args, res) => {
        if (args[0] == "stop") {
            res("Ok " + user + ", stopping..");
            return clearInterval(following[args[1] || user]);
        }

        res("Started following " + user);
        following[user] = setInterval(() => {
            bot.navigate.to(bot.players[args[1] || user].entity.position);
        }, args[0] || 5000);
    },
    "Starts following the player",
    ["followme"]
);

bot.command(
    "iloveu",
    (user, args, res) => {
        res("stfu " + user);
    },
    "It's self explanatory",
    ["ily"]
);

bot.command(
    "book",
    (user, args, res) => {
        bot.writeBook(36 - bot.quickBarSlot, [args[0]], err => {
            if (err) console.log(err);
            res("Added " + args[0] + " to my book.");
        });
    },
    "Makes the bot put something in the book it's holding",
    ["write"]
);

bot.command(
    "slot",
    (user, args, res) => {
        if (!args[0])
            return res("Currently holding slot " + bot.bot.quickBarSlot + ".");
        const slot = parseInt(args[0]);
        const item = bot.inventory.slots[36 + slot];
        if (slot > 8 || slot < 0) return res("Invalid slot");
        bot.setQuickBarSlot(slot);

        res(
            "Ok, holding " +
                slot +
                ", that is " +
                (item ? item.count + " of " + item.displayName : "nothing") +
                "."
        );
    },
    "Says the current slot, or sets it",
    ["hand"]
);

bot.command(
    "drop",
    (user, args, res) => {
        if (args[0] == "*" || !args[0]) {
            const items = bot.inventory.items();

            var i = 0;
            function depositItem() {
                if (items.length <= i) {
                    return res("Finished");
                }

                const item = items[i];
                setTimeout(
                    () => {
                        bot.tossStack(item, err => {
                            if (err) console.log(err);
                            i++;
                            depositItem();
                        });
                    },
                    parseInt(args[1]) < 20 ? 20 : args[1]
                );
            }

            depositItem();
            return;
        }
        var drop;
        bot.inventory.items().forEach(item => {
            if (item.name == args[0]) drop = item;
        });

        if (drop)
            bot.tossStack(drop, err => {
                if (err) res(JSON.stringify(err));
                else res("Done");
            });
        else res("Didn't find any " + args[0]);
        return;
    },
    "Drops a type of, or all items.",
    ["eject"]
);

bot.command(
    "coords",
    (user, args, res) => {
        function sayCoords(name, entity) {
            const pos = entity.position;
            return res(
                `${name}'s coords are: (x:${parseInt(pos.x)}, y:${parseInt(
                    pos.y
                )}, z:${parseInt(pos.z)})`
            );
        }
        if (!args[0] || args[0] == "you" || args[0] == "bot")
            sayCoords(bot.username, bot.bot.entity);
        else
            try {
                sayCoords(args[0], bot.bot.players[args[0]].entity);
            } catch (err) {
                res("I don't know " + args[0] + "'s coords, sorry :(");
            }
    },
    "Says the current coordinates",
    ["coord", "coordinates"]
);

bot.command(
    "hp",
    (user, args, res) => {
        const health = parseInt(bot.bot.health);
        res(parseInt(health) + " hp (" + health / 2 + " hearts)");
    },
    "Says the current health",
    ["health"]
);

bot.command(
    "swing",
    (user, args, res) => {
        if (!(args[0] == "" || args[0] == "right" || args[0] == "left"))
            return res("Invalid swing options");
        bot.swingArm(args[0]);
    },
    "Makes a hand-swing animation",
    ["fakehit"]
);

bot.command(
    "break",
    (user, args, res) => {
        if (!args[0] || !args[1] || !args[2]) return res("Invalid coordinates");
        const block = bot.blockAt(vec3(args[0], args[1], args[2]));
        const path = bot.navigate.findPathSync(
            vec3(block.position.x, block.position.y + 1, block.position.z)
        );
        bot.navigate.walk(path.path, reason => {
            res(reason);
            bot.dig(block, () => {
                res("Broke " + block.displayName);
            });
        });
    },
    "Breaks a block",
    ["destroy"]
);

bot.command(
    "chest",
    (user, args, res) => {
        const chestLoc = vec3(
            parseInt(args[0]),
            parseInt(args[1]),
            parseInt(args[2])
        );
        const path = bot.navigate.findPathSync(
            vec3(chestLoc.x, chestLoc.y + 1, chestLoc.z)
        );
        bot.navigate.walk(path.path, reason => {
            res("Got to the chest");
            const chest = bot.openChest(bot.blockAt(chestLoc));

            chest.on("open", () => {
                const invItems = bot.inventory.items();
                const cItems = chest.items();

                var i = 0;
                function depositItem() {
                    if (invItems.length <= i) {
                        chest.close();
                        return res("Finished");
                    }
                    const item = invItems[i];
                    setTimeout(() => {
                        chest.deposit(item.type, null, item.count, err => {
                            if (err) {
                                console.log(err);
                                return chest.close();
                            }
                            i++;
                            depositItem();
                        });
                    }, 100);
                }

                function withdrawItem() {
                    if (cItems.length <= i) {
                        chest.close();
                        return res("Finished");
                    }
                    const item = cItems[i];
                    setTimeout(() => {
                        chest.withdraw(item.type, null, item.count, err => {
                            if (err) {
                                console.log(err);
                                return chest.close();
                            }
                            i++;
                            withdrawItem();
                        });
                    }, 100);
                }

                if (
                    args[3] == "deposit" ||
                    args[3] == "put" ||
                    args[3] == "in" ||
                    !args[3]
                )
                    depositItem();
                if (
                    args[3] == "withdraw" ||
                    args[3] == "get" ||
                    args[3] == "out"
                )
                    withdrawItem();
            });
        });
    },
    "Gets or sets items of a chest",
    ["store", "storage"]
);

var dying;
bot.command(
    "rip",
    (user, args, res) => {
        if (args[0] == "stop") {
            dying = false;
            const playerPos = bot.players[user].entity.position;
            bot.lookAt(vec3(playerPos.x, playerPos.y + 1.8, playerPos.z));
            return res("Stopped dying");
        }
        const pos = bot.bot.entity.position;
        const timeout = args[0] || 500;

        function move() {
            if (!dying) return;
            bot.look(
                Math.random() * 360,
                -Math.PI / 2 + Math.random() * Math.PI,
                true
            );
            setTimeout(move, timeout);
        }

        dying = true;
        move();
        res("Started dying.");
    },
    "Rest in pieces",
    ["die", "begone"]
);

bot.command(
    "eat",
    (user, args, res) => {
        function eat() {
            bot.consume(err => {
                if (err) return console.log(err);
                else {
                    if (bot.bot.food >= 20) res("Finished eating.");
                    else eat();
                }
            });
        }
        eat();
    },
    "Eats the item the player's holding",
    ["consume"]
);

bot.command(
    "lookatme",
    (user, args, res) => {
        const playerPos = bot.players[user].entity.position;
        bot.lookAt(vec3(playerPos.x, playerPos.y + 1.8, playerPos.z));
    },
    "Looks in the eye of the player",
    ["look"]
);

bot.command(
    "sleep",
    (user, args, res) => {
        if (!args[0] || !args[1] || !args[2]) return res("Invalid coords");
        const bedPos = vec3(args[0], args[1], args[2]);

        const path = bot.navigate.findPathSync(
            vec3(bedPos.x, bedPos.y + 1, bedPos.z)
        );

        bot.navigate.walk(path.path, () => {
            if (bot.isABed(bot.blockAt(bedPos)))
                bot.sleep(bot.blockAt(bedPos), err => {
                    if (err) console.log(err);
                });
        });
    },
    "Makes the bot sleep in a bed",
    ["dream"]
);

bot.command(
    "hitme",
    (user, args, res) => {
        const playerPos = bot.players[user].entity.position;
        bot.lookAt(vec3(playerPos.x, playerPos.y + 1.8, playerPos.z));
        bot.swingArm(args[0]);
        bot.attack(bot.bot.players[user].entity);
        res("Hit! (or miss)");
    },
    "Makes the bot hit the player",
    ["hit"]
);

bot.command(
    "error",
    (user, args, res) => {
        throw "Test";
    },
    "Throws a test error"
);

bot.command(
    "deathMsg",
    (user, args, res) => {
        if (!args[0]) return res("Currently, deathMsg is " + deathMsg);

        const newValue = args[0] == "true";

        deathMsg = newValue;
        res("Set deathMsg to " + newValue);
    },
    "Tells you, or edits the current death message value",
    ["deathMessage"]
);

var deathMsg = true;
bot.bot.on("death", () => {
    if (deathMsg) bot.chat("Who killed me?! >:(");
});

bot.command(
    "craft",
    (user, args, res) => {
        const craftingTable = bot.blockAt(vec3(args[2], args[3], args[4]));
        const path = bot.navigate.findPathSync(
            vec3(
                craftingTable.position.x,
                craftingTable.position.y + 1,
                craftingTable.position.z
            )
        );
        bot.navigate.walk(path.path, reason => {
            res("Got to the crafting table");
            const id = args[0]
                ? args[0]
                : bot.inventory.slots[36 + bot.quickBarSlot].type;
            const recipe = bot.recipesFor(id, null, 1, craftingTable);

            bot.craft(recipe[0], args[1] || 1, craftingTable, () => {
                res("Done crafting.");
            });
        });
    },
    "Crafts an item",
    ["make", "create"]
);

bot.command(
    "wiki",
    (user, args, res) => {
        wiki()
            .page(args.join(" "))
            .then(page => {
                page.summary().then(sum => {
                    res(sum.substr(0, 251) + "[...]");
                });
            })
            .catch(err => {
                res("No such page");
                console.log(err);
            });
    },
    "Searches wikipedia",
    ["wikipedia", "whatis", "what"]
);

var isSprinkler = false;
bot.command(
    "sprinkler",
    (user, args, res) => {
        if (!devMode) return res("This feature is under development.");
        console.log(bot.bot.game.gameMode);

        if (args[0] == "stop") {
            isSprinkler = false;
            return res("Stopped being a sprinkler");
        }

        if (bot.bot.game.gameMode != 1 && bot.bot.game.gameMode != "creative")
            return res("I have to be in creative to be a sprinkler.");

        res("Started becoming a sprinkler");

        var stage = 0;

        function moveHead() {
            bot.look(
                Math.random() * 360,
                -Math.PI / 2 + Math.random() * Math.PI,
                true
            );
        }

        function dropItem() {
            try {
                bot.creative.setInventorySlot(
                    37,
                    bot.inventory.slots[36],
                    () => {
                        bot.tossStack(bot.inventory.slots[37], err => {
                            if (err) console.log(err);
                        });
                    }
                );
            } catch (err) {
                isSprinkler = false;
                res("Failed at being a sprinkler");
                console.log(err);
            }
        }

        isSprinkler = true;
        setInterval(() => {
            if (!isSprinkler) return;
            if (stage == 0) {
                moveHead();
                stage++;
            }
            if (stage == 1) {
                dropItem();
                stage = 0;
            }
        }, 150);
    },
    "Dying and infinitely dropping items",
    ["sprinkle"]
);

var devMode = false;

bot.command(
    "dev",
    (user, args, res) => {
        if (!args[0]) devMode = !devMode;
        else devMode = args[0] == "true";

        res("Devmode is now " + devMode);
        if (devMode)
            res(
                "Remember, it's only for developers. Please set it back if you don't know what is does."
            );
    },
    "Enables or disables devmode. ONLY FOR DEVS!",
    ["devmode", "developer"]
);

bot.command(
    "server",
    (user, args, res) => {
        ping(
            args[0],
            parseInt(args[1]) || 25565,
            { connectTimeout: 1000 * 15 },
            (error, r) => {
                if (error) {
                    console.log(error);
                    return res("Failed to ping  " + args[0]);
                }

                var description = "";

                var badChar = 0;
                r.descriptionText.split("").forEach(c => {
                    if (c == "§") {
                        return (badChar = 1);
                    }
                    if (badChar > 0) return badChar--;
                    description += c;
                });

                description = description.replace("\n", " ");

                res(
                    `Host: ${r.host}, ${
                        r.port == 25565 ? "" : `Port: ${r.port}, `
                    }Version ${r.version}, Players: ${r.onlinePlayers}/${
                        r.maxPlayers
                    }, Description: ${description}`
                );
            }
        );
    },
    "Pings a minecraft server",
    ["pingServer"]
);

bot.command(
    "alias",
    (user, args, res) => {
        if (bot.commands[args[0]].aliasOf) {
            res(args[0] + " is an alias of " + bot.commands[args[0]].aliasOf);
        } else {
            res(
                args[0] +
                    "'s aliases are: " +
                    bot.commands[args].aliases.join(", ")
            );
        }
    },
    "Get the original command of an alias",
    ["aliasOf"]
);

bot.alias("", "help");

bot.command(
    "mc",
    (user, args, reply) => {
        wiki({
            apiUrl: "https://minecraft.gamepedia.com/api.php",
            origin: null
        })
            .page(args.join(" "))
            .then(page => page.summary())
            .then(reply);
    },
    "Search the minecraft wiki",
    ["minecraft", "mcwiki", "mcw", "minecraftwiki"]
);
