module.exports = {
    version: "1.12.2",
    host: "localhost",
    port: 33097,
    name: "Botnet",
    prefix: "!",
    separator: " "
};
